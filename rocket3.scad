module c6_3() {
//    Gesamtimpuls: ca. 10.0 Ns, Schub: ca. 6.0 N
//    Schubdauer: ca. 1.6 s, Verzögerung: 3 s
translate([0,0,67.5/2])
    difference() {
    cylinder (h = 67.5, r=18/2, center = true,$fn=83);
     translate([0,0,67.5/2]) cylinder (h = 10, r=13/2, center = true,$fn=83);
    }
}
module payload() {
    union() {
       
        translate([0,0,100.0]) cylinder (h = 60, r=13.2/2, center = true,$fn=83);
        translate([0,0,95.5]) cylinder (h = 40, r=18/2, center = true,$fn=83);
    }
}
module finn() {
intersection (){
translate([0,0,40])rotate([-58,0,0])scale ([2,100,14]) sphere(1,$fn=83);
translate([-50,0,-20])
cube(70);
}
}
module rbody(){
translate([0,0,20]) difference(){
  union(){
 difference() {
    scale (13)translate([0,0,5]) {
        difference() {
        scale([1,1,7])  sphere(1.02,$fn=83);
        difference(){
            scale([1*0.8,1*0.8,7*0.90]) { sphere(1,$fn=83);}
            translate ([0,0,-4])cylinder ( h=10, r=1, center=true,$fn=83);
        }
         translate ([0,0,-4])cylinder ( h=10.5, r=.55,center=true,$fn=83);
    }
    };
    union() {
    translate([-50,-50,-100])   
             cube(100.1);
  cube(150); //cutaway)
     
    }
}
finn();
rotate([0,0,120]) finn();
rotate([0,0,-120]) finn();
}
c6_3();
payload();
}
}

//payload();
// c6_3();
module splice(){
union() {
translate([0,0,96/2]) cylinder(h=96,r=11,center=true);
translate([0,0,90/2]) cylinder(h=95,r=100,center=true);
}
}
//rbody();
//c6_3();
splice();
module test() rotate([0,0,17]) {

translate([0,20,96])
rotate([180,0,0])
intersection () {
  rbody();
  splice();  
}

translate([15,-20,-92.5])
difference () {
  rbody();
  splice();  
}

translate([-15,-20,0]) scale(17.5/18)
c6_3();
}
//translate ([00,0,0]) c6_3();


//http://www.openscad.org/documentation.html?version=2015.03
//http://www.rockets4schools.org/images/Basic.Rocket.Stability.pdf